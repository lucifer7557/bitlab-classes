import React, { Component } from 'react';
import { connect } from "react-redux";
import { bindActionCreators } from 'redux';
// import logo from './logo.svg';
import '../css/App.css';
import { returnData } from '../actions/indexs';

class App extends Component {

  componentDidMount() {
    console.log(this.props.animation);
    this.props.returnData();
  }

  render() {
    return (
      <div className="App">
        <div className="leftPanel">
          <div className="menuIcon"></div>
        </div>
        <div className="divPreview">
          <div className="divSubmit">
            Submit your work
          </div>
          <div className="divBody">
            <div className="mainIcon"></div>
            <div className="labelInspiration">
              Inspiration
            </div>
            {this.props.animation.length}
            <h2 className="mainHeading">Daily inspiration, right in your face.</h2>
            <div className="my_div">
              <label>Tags:</label>
              <input class="button" type="button" value="WebDesign" />
              <input class="button" type="button" value="Branding" />
              <input class="button" type="button" value="Illustration" />
              <input class="button1" type="button" value="Animation" />
              <span className="menu_grid">.</span>
            </div>
            <div className="divCards">
              <div class="card2 cardBox" >
                <img class="card-img-top" src="https://welovedaily.uk/data/1866866165965701088_3940206495.jpg" alt="Card image cap" />
                <div class="card-body">
                  {/* <p class="card-text">by Harish</p> */}
                  <span className="span1">by Harish</span>
                  <span className="span2">Dribble</span>
                </div>
              </div>
              <div class="card1 cardBox" >
                <img class="card-img-top" src="https://welovedaily.uk/data/1847297935899865211_3940206495.jpg" alt="Card image cap" />
                <div class="card-body">
                  <p class="card-text">by James</p>
                </div>
              </div>
              <div class="card3 cardBox" >
                <img class="card-img-top" src="https://welovedaily.uk/data/1878462297368445657_3940206495.jpg" alt="Card image cap" />
                <div class="card-body">
                  <p class="card-text">by Curran</p>
                </div>
              </div>
              <div class="card4 cardBox" >
                <img class="card-img-top" src="https://welovedaily.uk/data/1851643571822538067_3954040166.jpg" alt="Card image cap" />
                <div class="card-body">
                  <p class="card-text">by Harish</p>
                </div>
              </div>
              <div class="card5 cardBox" >
                <img class="card-img-top" src="https://welovedaily.uk/data/1850194043957176477_3954040166.jpg" alt="Card image cap" />
                <div class="card-body">
                  {/* <p class="card-text">by James</p> */}
                  <span className="span1">by James</span>
                  <span className="span2">dsgds</span>
                </div>
              </div>
              <div class="card6 cardBox" >
                <img class="card-img-top" src="https://welovedaily.uk/data/1848744484584357441_3954040166.jpg" alt="Card image cap" />
                <div class="card-body">
                  <p class="card-text">by Curran</p>
                </div>
              </div>
            </div>
          </div>

        </div>

      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log(state);
  return { animation: state.anim };
}

function mapDisptachToProps(dispatch) {
  return bindActionCreators({ returnData }, dispatch);
}

export default connect(mapStateToProps, mapDisptachToProps)(App);
