import {
    combineReducers
} from 'redux';
import animationData from './animation';

let rootReducer = combineReducers({
    anim: animationData
})

export default rootReducer;