import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
// import App from './App';
import App from './components/App';
import {
    applyMiddleware,
    createStore
} from 'redux';
import {
    Provider
} from 'react-redux';
import ReduxThunk from "redux-thunk";
import rootReducer from './reducers';

let createStoreWithMiddleware = applyMiddleware(ReduxThunk)(createStore);


// import * as serviceWorker from './serviceWorker';

ReactDOM.render(<Provider store={createStoreWithMiddleware(rootReducer)}>
    <App />
</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
// serviceWorker.unregister();