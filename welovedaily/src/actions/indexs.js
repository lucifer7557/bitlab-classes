import axios from 'axios';

export function returnData() {
    return (dispatch) => {
        dispatch({
            type: "NO_DATA_LOADED",
            data: []
        })
        axios.get('https://welovedaily.uk/an').then((data) => {
            console.log("action", data)
            dispatch({
                type: "ANIMATION_DATA",
                data: data.data
            })
        })
    }
}