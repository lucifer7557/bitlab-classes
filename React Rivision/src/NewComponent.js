import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class NewComponent extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="App">
                <h1>Hello {this.props.name}</h1>
                <img src="http://weclipart.com/gimg/745A22F375F423F6/vectorstock-199155-toddler-waving-vector.jpg" />
            </div>
        );
    }
}

export default NewComponent;
