import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NewComponent from './NewComponent';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      abc: "mona", sas: "masa", mm: 56, hell: {
        key: 1,
        name: "Hello"
      }
    }

    // this.changeABC = this.changeABC.bind(this);
  }

  changeABC() {
    this.setState({ abc: "Rapunzel" });
  }

  render() {
    return (
      <div className="App">
        <h1>Hello {this.state.abc}</h1>
        <button onClick={() => this.changeABC()}>Click Me!!!</button>
        <img src="http://weclipart.com/gimg/745A22F375F423F6/vectorstock-199155-toddler-waving-vector.jpg" />
        {/* <HelloWorld /> */}
        <NewComponent name='Praveen' />
      </div>
    );
  }
}

export default App;
