let data;

function loaded() {
    fetch('https://stumped.herokuapp.com/api/posts').then((res) => {
        return res.json();
    }).then((res) => {
        data = res;
        paintTheData(data, null);
    })
}

function categoryClick(category) {
    paintTheData(data, category);
}

function paintTheData(categoryData, category) {

    let divElement = document.getElementById("divData");
    console.log(divElement.innerHTML);
    divElement.innerHTML = "";
    if (category != null) {
        categoryData = categoryData.filter((abc) => {
            return abc.category == category;
        })
    }
    for (let i = 0; i < categoryData.length; i++) {
        let divParent = document.createElement('div');
        let img = document.createElement('img');
        let div = document.createElement('div');

        divParent.className = 'divCard';
        img.src = categoryData[i].mainImage;
        img.className = 'divCardImage';
        div.className = 'divCardText';
        div.innerHTML = categoryData[i].title;
        divParent.appendChild(img);
        divParent.appendChild(div);
        divElement.appendChild(divParent);
    }
}
