import React from 'react';
import { Link } from 'react-router-dom';

export default class Heroes extends React.Component {
    constructor() {
        super();

        this.state = {
            heroes: [
                {
                    key: 1,
                    name: 'Batman'
                },
                {
                    key: 2,
                    name: 'Flash'
                },
                {
                    key: 3,
                    name: 'Daredevil'
                }
            ]
        }
    }

    render() {
        return <div>
            {
                this.state.heroes.map((e) => {
                    return <div><Link to={'/heroes/' + e.name}>{e.name}</Link></div>
                })
            }
        </div>
    }
}