import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from './Home';
import Heroes from './Heroes';

let Main = function () {
    return <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/heroes" component={Heroes} />
    </Switch>

}

export default Main;