import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Link, Switch, Route } from 'react-router-dom';
import Home from './Home';
import HeroesPath from './HeroesPath';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="header">
          <Link to="/">Home</Link>
          <Link to="/heroes" className="ML30">Heroes</Link>
        </div>
        <div className="body">
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/heroes" component={HeroesPath} />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
