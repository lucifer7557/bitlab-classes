import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Heroes from './Heroes';
import HeroDetails from './HeroDetails';

let HeroesPath = function () {
    return <Switch>
        <Route exact path="/heroes" component={Heroes} />
        <Route path="/heroes/:name" component={HeroDetails} />
    </Switch>
}

export default HeroesPath;