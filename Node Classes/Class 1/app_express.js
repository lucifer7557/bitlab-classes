const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser())
app.get('/api/', function (req, res) {
    let data = fs.readFileSync("dummy.json");
    // console.log(data);
    let myNotes = JSON.parse(data);
    res.send(myNotes);
})

app.post('/api/addNote', function (req, res) {
    console.log(req.body);
    let data = fs.readFileSync("dummy.json");
    // console.log(data);
    let myNotes = JSON.parse(data);
    myNotes.push({
        title: req.body.title,
        body: req.body.body
    })
    fs.writeFileSync('dummy.json', JSON.stringify(myNotes));
    res.send({
        msg: "Success"
    });
})

// Ctrl+Shift+P Debug:Toggle Auto Attach

app.get('/api/about', function (req, res) {
    res.send({
        msg: "Nasas"
    });
})

app.listen(3000, () => {
    console.log("Running on Port ");
});