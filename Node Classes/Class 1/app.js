const os = require('os'); // importing a prebuilt package.
const fs = require('fs');
const _ = require('lodash'); // importing a npm package.
const yargs = require("yargs").argv;

// let arr = [1, 2, 3, 1, 5, 7, 2, 6];
// console.log(arr.indexOf(10));

// let dummy = [];
// for (let i = 0; i < arr.length; i++) {
//     if (dummy.indexOf(arr[i]) < 0) {
//         dummy.push(arr[i]);
//     }
// }

console.log(process.argv);
// node app.js add --title="First Note" --body="First Note In Mu Application" Addm Note
// node app.js remove --title="First Note"
// node app.js findAll 
// node app.js findOne --title="First Note"
// To get package.json do npm init
let data = fs.readFileSync("dummy.json");
console.log(data);
let myNotes = JSON.parse(data);
if (process.argv[2] == 'add') {
    console.log("Create a Note for To-Do Application");
    // let title = process.argv[3].split("=")[1];
    let title = yargs.title;
    let body = yargs.body;
    myNotes.push({
        title: title,
        body: body
    })
    fs.writeFileSync('dummy.json', JSON.stringify(myNotes));
} else if (process.argv[2] == 'remove') {
    console.log("Remove a Note");
} else if (process.argv[2] == 'findAll') {
    console.log("Find all Notes");
} else if (process.argv[2] == 'findOne') {
    console.log("Find a Note");
}

// fs.writeFileSync('dummy.json', JSON.stringify(os.userInfo()));

// console.log(dummy);

// console.log(_.uniq(arr));

// console.log(os.userInfo());

// To run a node application you have to use command "node fileName.js"

/*
 Advantages
1. Asynchronous NonBlocking IO
2. High Performance for Web Applications.
3. Single Threaded
*/